﻿using System;
using CoolParking.BL.Models;
using SimpleCMenu.Menu;
using System.Drawing;
using Console = Colorful.Console;
using System.Net.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;


namespace UserInterface
{
    class Program
    {
        static private HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            // Base address
            client.BaseAddress = new Uri("https://localhost:44363/");
            Console.SetWindowSize(72, 30);
            // Title word
            string headerText = "Cool Parking";
            Console.Clear();
            // Setup the menu
            ConsoleMenu mainMenu = new ConsoleMenu();
            // Transaction menu
            ConsoleMenu subMenu0 = new ConsoleMenu();
            subMenu0.SubTitle = "---------------------------- Меню Транзакцiй -----------------------------";
            subMenu0.AddMenuItem(0, "Поточнi транзакцiї", CurrentTransactions);
            subMenu0.AddMenuItem(1, "Iсторiя транзакцiй", TransactionHistory);
            subMenu0.AddMenuItem(2, "Вихiд", subMenu0.HideMenu);
            subMenu0.ParentMenu = mainMenu;
            // Vehicle menu
            ConsoleMenu subMenu1 = new ConsoleMenu();
            subMenu1.SubTitle = "---------------------------- Меню Транспорту -----------------------------";
            subMenu1.AddMenuItem(0, "Список транспорту на паркiнгу", VehicleList);
            subMenu1.AddMenuItem(1, "Поставити транспорт", AddVehicle);
            subMenu1.AddMenuItem(2, "Забрати транспорт", RemoveVehicle);
            subMenu1.AddMenuItem(3, "Поповнити транспортний засiб", TopUpVehicle);
            subMenu1.AddMenuItem(4, "Вихiд", subMenu1.HideMenu);
            subMenu1.ParentMenu = mainMenu;
            // Cool Parking title for all menues
            mainMenu.Header = headerText;
            subMenu0.Header = mainMenu.Header;
            subMenu1.Header = mainMenu.Header;
            // Main menu
            mainMenu.SubTitle = "-------------------------------- Menu ----------------------------------";
            mainMenu.AddMenuItem(0, "Поточний баланс", Balance);
            mainMenu.AddMenuItem(1, "Заробленi кошти", Money);
            mainMenu.AddMenuItem(2, "Вiльнi мiсця", FreePlaces);
            mainMenu.AddMenuItem(3, "Транзакцiї", subMenu0.ShowMenu);
            mainMenu.AddMenuItem(4, "Транспорт", subMenu1.ShowMenu);
            mainMenu.AddMenuItem(5, "Вихiд", Exit);
            // Display the menu
            mainMenu.ShowMenu();
        }

        //Methods
        public static void Balance()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            //var task = Task.Run(async () => await _client.GetStringAsync("https://localhost:44363/api/parking"));
            var balance = client.GetStringAsync("api/parking/balance").Result;
            Console.WriteLine($"Поточний баланс:{JsonConvert.DeserializeObject<decimal>(balance)}");
            Console.ReadKey();
        }

        public static void Money()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            decimal Sum = 0;
            var transactions = client.GetStringAsync("api/transactions/last").Result;
            foreach (var transaction in JsonConvert.DeserializeObject<TransactionInfo[]>(transactions))
            {
                Sum += transaction.Sum;
            }
            Console.WriteLine($"Заробленi кошти:{Sum}");
            Console.ReadKey();
        }

        public static void FreePlaces()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            var freePlaces = client.GetStringAsync("api/parking/freeplaces").Result;
            Console.WriteLine($"Вiльнi мiсця:{JsonConvert.DeserializeObject<decimal>(freePlaces)}");
            Console.ReadKey();
        }

        public static void CurrentTransactions()
        {
            Console.WriteLine("Для повернення у МЕНЮ натичнiть будь-яку клавiшу\n", Color.Khaki);
            var transactions = client.GetStringAsync("api/transactions/last").Result;
            foreach (var transaction in JsonConvert.DeserializeObject<TransactionInfo[]>(transactions))
            {
                Console.WriteLine($"Id транспорту:{transaction.Id} Сума:{transaction.Sum} Час:{transaction.Time}");
            }
            Console.ReadKey();
        }

        public static void TransactionHistory()
        {
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            var transactions = client.GetAsync("api/transactions/all").Result;
           if (transactions.IsSuccessStatusCode)
            {
                Console.WriteLine(JsonConvert.DeserializeObject<string>(transactions.Content.ReadAsStringAsync().Result));
                Console.WriteLine("Операцiя виконана успiшно!", Color.Green);
            }
            else
            {
                Console.WriteLine(transactions.Content.ReadAsStringAsync().Result, Color.Red);
            }
            Console.ReadKey();
        }

        public static void VehicleList()
        {
            var vehicles = JsonConvert.DeserializeObject<List<Vehicle>>(client.GetStringAsync("api/vehicles").Result);
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            if (vehicles.Count == 0)
            {
                Console.WriteLine("Немає транспорту на парковцi", Color.Red);
            }
            else
            {
                foreach (var vehicle in vehicles)
                {
                    Console.WriteLine($"Id транспорту:{vehicle.Id} Тип транспорту:{vehicle.VehicleType} Баланс:{vehicle.Balance}");
                }
            }
            Console.ReadKey();
        }

        public static void AddVehicle()
        {
            Console.SetWindowSize(72, 30);
            // User input
            Console.WriteLine("Введiть Id:");
            string vehicleId = Console.ReadLine();
            Console.WriteLine("Введiть тип транспорту:\n1-легковий автомобiль,2-вантажiвка,3-автобус,4-мотоцикл");
            int type = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введiть баланс:");
            decimal vhecileBalance = Convert.ToDecimal(Console.ReadLine());
            // Serializing with creating  anonymous object
            var json = JsonConvert.SerializeObject(new { id = vehicleId,  vehicleType = type, balance = vhecileBalance });
            // Creating content for Post request
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            // Post request
            var response = client.PostAsync("api/vehicles", content).Result;
            Console.WriteLine("Для повернення у МЕНЮ натиснiть будь-яку клавiшу\n", Color.Khaki);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Операцiя виконана успiшно!", Color.Green);
            }
            else
            {
                Console.WriteLine(response.Content.ReadAsStringAsync().Result, Color.Red);
            }
            Console.ReadKey();
        }

        public static void RemoveVehicle()
        {
            Console.SetWindowSize(72, 30);
            Console.WriteLine("Для повернення у меню нажмiть будь-яку клавiшу\n", Color.Khaki);
            Console.WriteLine("Введiть ID транспорту");
            string id = Console.ReadLine();
            var response = client.DeleteAsync("api/vehicles/" + id).Result;
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("ОперацIя виконана успiшно!", Color.Green);
            }
            else
            {
                Console.WriteLine(response.Content.ReadAsStringAsync().Result, Color.Red);
            }
            Console.ReadKey();
        }

        public static void TopUpVehicle()
        {
            Console.SetWindowSize(72, 30);
            // User input
            Console.WriteLine("Введiть Id:");
            string vehicleId = Console.ReadLine();
            Console.WriteLine("Введiть суму поповнення:");
            decimal vehicleSum = Convert.ToDecimal(Console.ReadLine());
            // Serializing 
            var json = JsonConvert.SerializeObject(new { id = vehicleId, Sum = vehicleSum });
            // Creating content for Post request
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            // Post request
            var response = client.PutAsync("api/transactions/topUpVehicle", content).Result;
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Операцiя виконана успiшно!", Color.Green);
            }
            else
            {
                Console.WriteLine(response.Content.ReadAsStringAsync().Result, Color.Red);
            }
            Console.ReadKey();
        }
        public static void Exit()
        {
            Environment.Exit(0);
        }
    }
}
