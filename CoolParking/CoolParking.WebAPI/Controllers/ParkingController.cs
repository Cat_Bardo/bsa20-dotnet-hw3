﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;

        public ParkingController(ParkingService service)
        {
            parkingService = service;
        }

        // GET: api/parking/balance
        [HttpGet("parking/balance")]
        public IActionResult GetBalance()
        {
            return Ok(JsonConvert.SerializeObject(parkingService.GetBalance()));
        }

        //api/parking/capacity
        [HttpGet("parking/capacity")]
        public IActionResult GetCapacity()
        {
            return Ok(parkingService.GetCapacity());
        }

        //api/parking/freeplaces
        [HttpGet("parking/freePlaces")]
        public IActionResult GetFreePlaces()
        {
            return Ok(parkingService.GetFreePlaces());
        }

        //GET: api/vehicles
        [HttpGet("vehicles")]
        public IActionResult GetVehicles()
        {
            return Ok(parkingService.GetVehicles());
        }

        //GET: api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpGet("vehicles/{id}")]
        public IActionResult GetVehicle(string id)
        {
            if (Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$"))
            {
                foreach (var vehicle in parkingService.GetVehicles())
                {
                    if (vehicle.Id == id)
                    {
                        return Ok(vehicle);
                    }
                }
                return NotFound("Транспорт вiдсутнiй!");
            }
            return BadRequest("Не коректний id!");
        }

        ////POST: api/vehicles
        [HttpPost("vehicles")]
        public IActionResult AddVehicle([FromBody]object content)
        {
            try
            {
                JsonSchema schema = JsonSchema.Parse(@"{'type': 'object',
                                                        'properties': {'id': {'type': 'string', 'pattern': '^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$'},
                                                                       'vehicleType':{'type': 'integer','maximum': 4, 'minimum': 1},
                                                                       'balance': {'type': 'number', 'minimum': 0}},
                                                        'additionalProperties': false}");
                JObject jObject = JObject.Parse($@"{content.ToString()}");
                if (jObject.IsValid(schema))
                {
                    Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(content.ToString());
                   /* if (vehicle.Id == null || vehicle.Balance < 0 || (int)vehicle.VehicleType < 0 || (int)vehicle.VehicleType > 4)
                    {
                        throw new InvalidOperationException("Не валiдний запит");
                    }*/
                    parkingService.AddVehicle(vehicle);
                    return Created("api/vehicles", vehicle);
                }
                return BadRequest("Не валiдний запит!");
            }
            catch (JsonSerializationException)
            {
                return BadRequest("Не валiдний запит!");
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }

        //DELETE: api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [HttpDelete("vehicles/{id}")]
        public IActionResult RemoveVehicle(string id)
        {
            try
            {
                if (Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$"))
                {
                    foreach (var vehicle in parkingService.GetVehicles())
                    {
                        if (vehicle.Id == id)
                        {
                            parkingService.RemoveVehicle(id);
                            return NoContent();
                        }
                    }
                    return NotFound("Транспорт вiдсутнiй!");
                }
                return BadRequest("Не валiдний id!");
            }
            catch (ArgumentException)
            {
                return NotFound("Не коректний id!");
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Баланс не може бути негативний!");
            }
        }

        //GET: api/transactions/last
        [HttpGet("transactions/last")]
        public IActionResult GetLastTransactions()
        {
            return Ok(parkingService.GetLastParkingTransactions());
        }

        //GET: api/transactions/all
        [HttpGet("transactions/all")]
        public IActionResult GetAllTransactions()
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(parkingService.ReadFromLog()));
            }
            catch (InvalidOperationException)
            {
                return NotFound("Записи транзакцiй вiдстутнi!");
            }
        }

        //PUT: api/transactions/topUpVehicle
        [HttpPut("transactions/topUpVehicle")]
        public IActionResult TopUpVehicle([FromBody] object content)
        {
            try
            {
                JsonSchema schema = JsonSchema.Parse(@"{'type': 'object',
                                                        'properties': {'id': {'type': 'string', 'pattern': '^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$'},
                                                                       'Sum': {'type': 'number','minimum': 0}},
                                                        'additionalProperties': false}");
                JObject jObject = JObject.Parse($@"{content.ToString()}");
                if (jObject.IsValid(schema))
                {
                    TempObject data = JsonConvert.DeserializeObject<TempObject>(content.ToString());
                    parkingService.TopUpVehicle(data.Id, data.Sum);
                    Vehicle vehicle = null;
                    foreach (var v in parkingService.GetVehicles())
                    {
                        if (data.Id == v.Id)
                        {
                            vehicle = v;
                            return Ok(JsonConvert.SerializeObject(vehicle));
                        }
                    }
                    return BadRequest("Транспорт відсутній!");
                }
                return BadRequest("Не валiдний запит!");
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (InvalidOperationException e)
            {
                return BadRequest(e.Message);
            }
        }
        class TempObject
        {
            public string Id { get; set; }
            public decimal Sum { get; set; }
            public TempObject(string id, decimal sum) { Id = id; Sum = sum; }
        }
    }
}
