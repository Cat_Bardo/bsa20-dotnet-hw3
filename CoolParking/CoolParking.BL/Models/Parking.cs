﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; }
        public List<Vehicle> vehicles = new List<Vehicle>();
        private static Parking parking;
        private Parking(){}

        public static Parking getInstance()
        {
            if (parking == null)
                parking = new Parking();
            return parking;
        }
    }
}