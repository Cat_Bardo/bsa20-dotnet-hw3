﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance {get; internal set;}
        string pattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";
        public static Random rand = new Random((int)DateTime.Now.Ticks);
        public static char[]  letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

        [JsonConstructor]
        public Vehicle(string id, int vehicleType, decimal balance) :this(id,(VehicleType)vehicleType,balance){}
        public Vehicle(string id, VehicleType vt, decimal balance)
        {
            if (balance <= 0) { throw new ArgumentException("\nНегативний баланс неможливий"); }
            if (Regex.IsMatch(id, pattern))
            {
                this.Id = id;
                this.VehicleType = vt;
                this.Balance = balance;
            }
            else throw new ArgumentException("\nНе вiрно заданий Id ");
        }
        private static char RandChar() // Method to generate random char from A to Z
        {
            return (char)rand.Next('A', 'Z' + 1);
        }

        private static int RandNumber() // Method to generate random number from 0 to 9
        {
            return rand.Next(0, 9);
        }
        public static string GenerateRandomRegistrationPlateNumber() // Method create random string in format @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}"
        {
            string result = $"{RandChar()}{RandChar()}-{RandNumber()}{RandNumber()}{RandNumber()}{RandNumber()}-{RandChar()}{RandChar()}";
            return result;
        }

    }
}