﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.IO;

public struct TransactionInfo 
{
    public string Id { get; set; }
    public decimal Sum { get; set; }
    public DateTime Time { get; set; }
    public TransactionInfo(string id, decimal sum, DateTime time) 
    {
        this.Id = id;
        this.Sum = sum;
        this.Time = time;
    }
}