﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        ILogService logService;
        readonly ITimerService logTimer;
        readonly ITimerService rentTimer;
        List<TransactionInfo> transactions = new List<TransactionInfo>();
        Parking parking = Parking.getInstance();

        public ParkingService() : this(new TimerService(), new TimerService(), new LogService("Transaction.log")) { }

        public ParkingService(ITimerService rentTimer, ITimerService logTimer, ILogService logService)
        {
            parking.vehicles = new List<Vehicle>();
            this.logService = logService;
            this.logTimer = logTimer;
            this.rentTimer = rentTimer;
            this.rentTimer.Interval = Settings.rentTime;
            this.logTimer.Interval = Settings.logTime;
            this.rentTimer.Elapsed += rentHendler;
            this.logTimer.Elapsed += logHandler;
            this.rentTimer.Start();
            this.logTimer.Start();
        }
        // handler for rentTimer
        public void rentHendler(object sender, ElapsedEventArgs e)
        {
            decimal sum;
            foreach (var vehicle in parking.vehicles)
            {
                if (vehicle.Balance <= 0)
                {
                    sum = Settings.rate[vehicle.VehicleType] * Settings.taxKoef;
                }
                else if ((vehicle.Balance - Settings.rate[vehicle.VehicleType]) < 0)
                {
                    sum = vehicle.Balance + (Settings.rate[vehicle.VehicleType] - vehicle.Balance) * Settings.taxKoef;
                }
                else
                {
                    sum = Settings.rate[vehicle.VehicleType];
                }
                vehicle.Balance -= sum;
                parking.Balance += sum;
                transactions.Add(new TransactionInfo(vehicle.Id, sum, DateTime.Now));
            }
        }
        // handler for logTimer
        public void logHandler(object sender, ElapsedEventArgs e)
        {
            string logInfo = "";
            foreach (TransactionInfo tr in transactions)
            {
                logInfo += $"{tr.Time} {tr.Id} {tr.Sum}\n";
            }
            logService.Write(logInfo);
            transactions.Clear();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }
        public int GetCapacity()
        {
            return Settings.capacity;
        }
        public int GetFreePlaces()
        {
            return Settings.capacity - parking.vehicles.Count;
        }
        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.vehicles);
        }
        public void AddVehicle(Vehicle vehicle)
        {
            foreach (var v in parking.vehicles)
            {
                if (v.Id == vehicle.Id) { throw new ArgumentException("Транспорт з даним id вже iснує!"); }
            }
            if (GetFreePlaces() > 0) // if parking has free place
            {
                parking.vehicles.Add(vehicle);
            }
            else { throw new InvalidOperationException("Немає вільних місць!"); }
        }
        public void RemoveVehicle(string vehicleId)
        {
            foreach (var k in parking.vehicles)
            {
                if (k.Id == vehicleId)
                {
                    if (k.Balance >= 0) // vehicle balance must be positive
                    {
                        parking.vehicles.Remove(k);
                        return;
                    }
                    else { throw new InvalidOperationException(); }
                }
            }
            throw new ArgumentException();
        }
        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0) { throw new ArgumentException(); }
            foreach (var k in parking.vehicles)
            {
                if (k.Id == vehicleId)
                {
                    k.Balance += sum;
                    return;
                }
            }
            throw new ArgumentException();
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }
        public string ReadFromLog()
        {
            return logService.Read();

        }
        public void Dispose()
        {
            logTimer.Stop();
            rentTimer.Stop();
            transactions.Clear();
            parking.Balance = 0;
            parking.vehicles = null;
            GC.SuppressFinalize(this);
        }
    }
}